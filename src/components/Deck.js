import {Card, Container} from 'react-bootstrap';
import { AvailableWrestlers } from './AvailableWrestlers';
import React, { useState } from 'react';
import deckBack from "../images/deckBack1.jpg";


export const Deck=({data, seedRange, year, numShow, pointsCost, showFinalScore})=>{

    function shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }   

 

    const [shuffledDeck, setShuffledDeck]=useState(shuffleArray(data.filter(d=>d.Year===year && d.Seed>=seedRange[0] && d.Seed<=seedRange[1])));
 

    const updateDeck = (wrestler)=>{ 
        setShuffledDeck(shuffledDeck.filter(w=>w.ID!==wrestler.ID));
        console.log(wrestler.FirstName, wrestler.LastName);

    }

    return (
        <Container style={{display: 'flex', flexDirection: 'row'}}> 
            <AvailableWrestlers wrestlers={shuffledDeck.slice(0,numShow)} pointsCost={pointsCost} showFinalScore={showFinalScore} updateDeck={updateDeck}/>
            <Card style={{ width: '16rem' }} className="card" >
                <Card.Img className="image-fluid h-100 w-100" src= {deckBack}/>    
            </Card>   
        </Container> 
    )




    


    
}