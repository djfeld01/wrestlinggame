import { useState, useEffect } from 'react';
import { csv } from 'd3';
import { v4 as uuidv4 } from 'uuid';

const csvUrl =
    'https://gist.githubusercontent.com/djfeld01/4e04cf1a42858aed30d14275a04c22c8/raw/999a520739d2213ab456a9b5265e200c0671c569/ncaaWrestling.csv';

export const useData=()=>{
    const [data, setData]= useState(null);

    useEffect(()=> {
        const row= (d)=> {
            const fullName=d.Name.split(', ');
            d.FirstName=fullName[1];
            d.LastName=fullName[0];
            d.ID= uuidv4();
            d.Seed=  +d.Seed;
            d.Weight= +d.Weight;
            d.Points= +d.Points;
            d.Year= +d.Year;
            return d
        }
        csv(csvUrl,row).then(setData);
        },[]);

          

        return data;
    }