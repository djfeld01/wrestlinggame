import React, { useState } from 'react';
import {Card} from 'react-bootstrap';
//import { useState } from 'react';


export const AvailableWrestlers=({wrestlers, pointsCost, showFinalScore, updateDeck})=>{
    

    return wrestlers.map((wrestler, index)=> 
    
    <Card onClick={()=> updateDeck(wrestler)} key={wrestler.ID} style={{ width: '16rem' }} className="card">
        
        <Card.Header><div className="text-dark text-center h6">{wrestler.FirstName} {wrestler.LastName}</div></Card.Header>
             
        <Card.Body className="text-dark text-center h6">
            
            <Card.Title>
                Seed: {wrestler.Seed}
            </Card.Title> 

            <Card.Text>
                Weight: {wrestler.Weight} 
            </Card.Text>
            <Card.Text>{wrestler.Team}</Card.Text>
            <Card.Text>
                {showFinalScore ? "Final Points: "+ wrestler.Points : "" }
            </Card.Text> 
            <Card.Text>
                {showFinalScore ? "Net Points: " + (wrestler.Points + pointsCost[index]) : ""}
            </Card.Text>
        </Card.Body>
        
        <Card.Footer>
            <div className="text-muted text-center h7">
                {pointsCost[index] < 0 ? "Lose " + Math.abs(pointsCost[index]) : "Gain " + pointsCost[index]}
            </div>
        </Card.Footer>   
    </Card>
    )
}