import React, { useState } from 'react';
import { AvailableWrestlers } from './components/AvailableWrestlers';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useData } from './components/useData';
import { Container, DropdownButton, Dropdown } from 'react-bootstrap';
import {Deck} from './components/Deck';
//import { Dropdown, DropdownButton } from 'bootstrap';



const App=()=>{
  
  const [gameYear, setGameYear]=useState(2021);

  const [showScore, setShowScore]=useState(false);


  const handleClick=()=>setShowScore (!showScore);



//get Data from csv
  const data=useData();

  if (!data) {
    return <pre>Loading... </pre>
  }




  const pointsCostHighSeeds=[0,-2,-4,-6,-8,-10,-12,-14,-16,-20];
  const pointsCostLowSeeds=[12,10,8,6,4,2,0,-2,-4];

  return (
    <div>
      
      <button onClick={handleClick} >{showScore ? "Hide Score" : "Show Score" }</button>
      <DropdownButton id="dropdown-basic-button" title="Year">
        {[2014,2015,2016,2017,2018,2019,2021].map(
          (year)=>(
            <Dropdown.Item onClick={()=>setGameYear(year)}>{year}</Dropdown.Item>
          )
        )}
      </DropdownButton>
     
      <Deck data={data} seedRange={[0,4]} year= {gameYear} numShow={4} pointsCost={pointsCostHighSeeds} showFinalScore={showScore}/>
      <Deck data={data} seedRange={[5,12]} year={gameYear} numShow={5} pointsCost={pointsCostLowSeeds} showFinalScore={showScore} />
    </div>
  )
}

export default App;